// bai 1

function tinhLuong() {
    event.preventDefault();
    var luongMotNgay = document.getElementById('luongMotNgay').value * 1
    var soNgayLam = document.getElementById('soNgayLam').value * 1
    var result = luongMotNgay * soNgayLam

    document.getElementById('tinhluong').innerHTML = `<h1>Tiền lương của bạn là: ${result.toLocaleString()}</h1>`
}

// bài 2
function tinhTrungBinh() {
    event.preventDefault()
    var soThuNhat = document.getElementById('soThuNhat').value * 1
    var soThuHai = document.getElementById('soThuHai').value * 1
    var soThuBa = document.getElementById('soThuBa').value * 1
    var soThuTu = document.getElementById('soThuTu').value * 1
    var soThuNam = document.getElementById('soThuNam').value * 1
    var result = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5

    document.getElementById('trungbinh').innerHTML = `<h1>Giá trị trung bình là: ${result.toFixed(2)}</h1>`


}
// bài 3
function quyDoiTien() {
    event.preventDefault()
    var vnd = new Intl.NumberFormat('vn-VN').format(23500)
    var usd = document.getElementById('usd').value * 1
    var result = vnd * usd

    document.getElementById('doitien').innerHTML = `<h1>${result.toFixed(3)} vnd</h1>`
}
// Bài 4
function chuViDienTichhinhChuNhat() {
    event.preventDefault()

    var chieudai = document.getElementById('chieudai').value * 1
    var chieurong = document.getElementById('chieurong').value * 1
    var chuVi = (chieurong + chieudai) * 2
    console.log('chuVi: ', chuVi);
    var dienTich = chieudai * chieurong

    document.getElementById('chuvi').innerHTML = `<h1>Chu vi hình chữ nhật là: ${chuVi.toFixed(2)} </h1>`
    document.getElementById('dientich').innerHTML = `<h1>Diện tích hình chữ nhật là: ${dienTich.toFixed(2)} </h1>`
}
// bài 5
function tongHaiKySo() {
    event.preventDefault()
    var kySo = document.getElementById('kyso').value * 1
    var hangDonVi = kySo % 10
    var hangChuc = Math.floor(kySo / 10)
    var result = hangDonVi + hangChuc

    document.getElementById('tinh-tong-ky-so').innerHTML = `<h1>Tổng hai ký số là: ${result}</h1>`
}